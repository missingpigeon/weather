import MainView from './components/layout/MainView';

function App() {
	return (
		<>
			<MainView />
		</>
	);
}

export default App;
