export type AppSettings = {
    units: string;
    timeFormat: string;
    autoRefresh: number;
    showVFX: boolean;
}
