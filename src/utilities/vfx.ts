import { getRandomIntInRange } from './numbers';
import WeatherEffects from '@/json/vfx-config.json';
import SkyPalettes from '@/json/sky-colors.json';

// Weather VFX import and initialization
export type VFXConfig = {
	clouds: {
		img: string;
		repeat: boolean;
		repeatImg: string | null;
		windSpeed: number;
		opacity: number;
	}[];
	rain?: {
		count: number;
		speed: number;
		opacity: number;
	};
	snow?: {
		count: number;
		speed: number;
		opacity: number;
	};
	lightning?: {
		interval: number;
		chance: number;
	};
};

export type SkyPalette = {
	night: [string, string, string];
	day: [string, string, string];
	sunrise: [string, string, string];
	sunset: [string, string, string];
};

export const constructVFX = (
	condition: number,
	timestamps: { dt: number; sunrise: number; sunset: number },
) => {
	let conditionKey = 'clear';
	if (condition === 804) conditionKey = 'overcast';
	else if (condition === 800) conditionKey = 'clear';
	else {
		const conditionGroup = Math.floor(condition / 100) * 100;
		switch (conditionGroup) {
			case 200:
				conditionKey = 'thunderstorm';
				break;
			case 300:
				conditionKey = 'clouds';
				break;
			case 500:
				conditionKey = 'rain';
				break;
			case 600:
				conditionKey = 'snow';
				break;
			case 700:
				conditionKey = 'fog';
				break;
			case 800:
				conditionKey = 'clouds';
				break;
			default:
				conditionKey = 'clear';
		}
	}
	const currentDate = new Date(timestamps.dt * 1000);
	const sunriseDate = new Date(timestamps.sunrise * 1000);
	const sunsetDate = new Date(timestamps.sunset * 1000);
	const currentHour = currentDate.getUTCHours();
	const sunriseHour = sunriseDate.getUTCHours();
	const sunsetHour =
		sunsetDate.getUTCHours() === 0 ? 24 : sunsetDate.getUTCHours();
	if (Math.abs(timestamps.dt - timestamps.sunrise) <= 3600)
		return {
			sky: SkyPalettes[conditionKey as keyof typeof SkyPalettes].sunrise,
			animations:
				WeatherEffects[conditionKey as keyof typeof WeatherEffects],
			stars: true
		};
	if (Math.abs(timestamps.dt - timestamps.sunset) <= 3600)
		return {
			sky: SkyPalettes[conditionKey as keyof typeof SkyPalettes].sunset,
			animations:
				WeatherEffects[conditionKey as keyof typeof WeatherEffects],
			stars: true
		};
	if (currentHour < sunriseHour || currentHour > sunsetHour)
		return {
			sky: SkyPalettes[conditionKey as keyof typeof SkyPalettes].night,
			animations:
				WeatherEffects[conditionKey as keyof typeof WeatherEffects],
			stars: true
		};
	return {
		sky: SkyPalettes[conditionKey as keyof typeof SkyPalettes].day,
		animations: WeatherEffects[conditionKey as keyof typeof WeatherEffects],
		stars: false
	};
};

// Draw weather animations on the canvas
abstract class VFXAsset {
	canvas: HTMLCanvasElement | null;
	img: HTMLImageElement | null;
	x: number;
	y: number;
	width: number;
	height: number;
	opacity: number;
	xVelocity?: number;
	yVelocity?: number;

	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		x: number,
		y: number,
		width: number,
		height: number,
		opacity: number,
	) {
		this.canvas = canvas;
		this.img = img;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.opacity = opacity;
		this.xVelocity = 0;
		this.yVelocity = 0;
	}

	draw() {
		if (this.canvas && this.img) {
			if (this.xVelocity) this.x += this.xVelocity;
			if (this.yVelocity) this.y += this.yVelocity;
			(
				this.canvas.getContext('2d') as CanvasRenderingContext2D
			).drawImage(
				this.img,
				0,
				0,
				this.img.width,
				this.img.height,
				this.x,
				this.y,
				this.width,
				this.height,
			);
			if (this.x > this.canvas.width) {
				this.x = 0 - this.width;
			}
			if (this.y > this.canvas.height) {
				this.y = 0 - this.height;
			}
			return true;
		} else return false;
	}
}

class VFXCloud extends VFXAsset {
	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		x: number,
		y: number,
		width: number,
		height: number,
		opacity: number,
		windSpeed: number,
	) {
		super(canvas, img, x, y, width, height, opacity);
		this.xVelocity = windSpeed / 16;
	}

	draw() {
		if (this.canvas && this.img) {
			const context = this.canvas.getContext(
				'2d',
			) as CanvasRenderingContext2D;
			if (this.xVelocity) this.x += this.xVelocity;
			context.globalAlpha = this.opacity / 100;
			context.drawImage(
				this.img,
				0,
				0,
				this.img.width,
				this.img.height,
				this.x,
				this.y,
				this.width,
				this.height,
			);
			context.globalAlpha = 1;
			if (this.x > this.canvas.width) {
				this.x = 0 - this.width;
			}
			return true;
		} else return false;
	}
}

class VFXRaindrop extends VFXAsset {
	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		x: number,
		y: number,
		width: number,
		height: number,
		opacity: number,
	) {
		super(canvas, img, x, y, width, height, opacity);
		this.xVelocity = 0;
		this.yVelocity = 30;
	}

	draw() {
		if (this.canvas) {
			const context = this.canvas.getContext(
				'2d',
			) as CanvasRenderingContext2D;
			context.globalAlpha = this.opacity / 100;
			if (this.yVelocity) this.y += this.yVelocity;
			if (this.img)
				/* context.drawImage(
					this.img,
					0,
					0,
					this.img.width,
					this.img.height,
					this.x,
					this.y,
					this.width,
					this.height,
				); */
				context.drawImage(this.img, this.x, this.y);
			else {
				context.fillStyle = `#ffffff`;
				context.fillRect(this.x, this.y, this.width, this.height);
			}
			context.globalAlpha = 1;
			if (this.y > this.canvas.height) {
				return false;
			}
			return true;
		} else return false;
	}
}

class VFXSnowflake extends VFXAsset {
	sway = false;

	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		x: number,
		y: number,
		width: number,
		height: number,
		opacity: number,
		windSpeed: number,
	) {
		super(canvas, img, x, y, width, height, opacity);
		this.xVelocity =
			(windSpeed - getRandomIntInRange(0, Math.floor(windSpeed))) / 10;
		this.yVelocity = getRandomIntInRange(0.8, 1.4);
		if (Math.floor(windSpeed) < 3) this.sway = true;
	}

	draw() {
		if (this.canvas) {
			const context = this.canvas.getContext(
				'2d',
			) as CanvasRenderingContext2D;
			context.globalAlpha = this.opacity / 100;
			if (this.xVelocity) {
				if (this.sway && Math.random() * 10 > 9.5) {
					this.x -= this.xVelocity / 2;
				} else this.x += this.xVelocity;
			}
			if (this.yVelocity) this.y += this.yVelocity;
			if (this.img)
				context.drawImage(
					this.img,
					0,
					0,
					this.img.width,
					this.img.height,
					this.x,
					this.y,
					this.width,
					this.height,
				);
			else {
				context.fillStyle = `#ffffff`;
				context.arc(
					this.x,
					this.y,
					this.width / 2,
					0,
					2 * Math.PI,
					false,
				);
				context.fill();
			}
			context.globalAlpha = 1;
			if (this.x > this.canvas.width || this.y > this.canvas.height) {
				return false;
			}
			return true;
		} else return false;
	}
}
class VFXLightningBolt extends VFXAsset {
	frameTime: number;
	duration: number;
	flashDuration: number;
	fadeDuration: number;
	maxDuration: number;
	flashOpacity: number;
	boltCanvas: HTMLCanvasElement;

	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		x: number,
		y: number,
		width: number,
		height: number,
		opacity: number,
	) {
		super(canvas, img, x, y, width, height, opacity);
		this.flashDuration = 0.4;
		this.fadeDuration = 0.6;
		this.maxDuration = this.flashDuration + this.fadeDuration;
		this.duration = 0;
		this.frameTime = new Date().getTime();
		this.flashOpacity = 0.15 + Math.random() * 0.25;
		this.boltCanvas = document.createElement('canvas') as HTMLCanvasElement;
		if (this.canvas) {
			this.boltCanvas.width = window.innerWidth;
			this.boltCanvas.height = window.innerHeight;
		}
	}

	draw() {
		const currentFrameTime = new Date().getTime();
		const elapsed = (currentFrameTime - this.frameTime) / 1000;
		this.duration += elapsed;
		this.frameTime = currentFrameTime;
		if (this.canvas) {
			const context = this.canvas.getContext(
				'2d',
			) as CanvasRenderingContext2D;
			context.globalAlpha = Math.max(
				0,
				Math.min(
					1,
					(this.maxDuration - this.duration) / this.fadeDuration,
				),
			);
			context.drawImage(
				this.boltCanvas as CanvasImageSource,
				0,
				0,
				this.canvas.width,
				this.canvas.height,
			);
			if (this.flashOpacity > 0) {
				context.globalAlpha = 1;
				context.fillStyle = `rgba(255, 255, 255, ${this.flashOpacity})`;
				context.fillRect(0, 0, this.canvas.width, this.canvas.height);
				this.flashOpacity = Math.max(0, this.flashOpacity - elapsed);
			}
		}
		if (this.duration >= this.maxDuration) {
			return false;
		} else return true;
	}

	recursiveDraw(x: number, y: number, length: number, direction: number) {
		const originalDirection = direction;
		const maxLength = this.boltCanvas.height;
		const interval = window.setInterval(() => {
			if (length <= 0) {
				clearInterval(interval);
				return;
			}

			while (length > 0) {
				const originalX = Math.floor(x);
				const originalY = Math.floor(y);
				x += Math.cos(direction);
				y -= Math.sin(direction);
				length--;

				if (
					originalX !== Math.floor(x) ||
					originalY !== Math.floor(y)
				) {
					const alpha = Math.min(1, length / maxLength);
					const context = this.boltCanvas.getContext(
						'2d',
					) as CanvasRenderingContext2D;
					context.strokeStyle = `rgba(255, 255, 255, ${alpha})`;
					context.shadowColor = 'rgba(217, 186, 255, 1)';
					context.shadowBlur = 6;
					context.beginPath();
					context.moveTo(originalX, originalY);
					context.lineTo(x, y);
					context.stroke();

					direction =
						originalDirection +
						(-Math.PI / 8.0 + Math.random() * (Math.PI / 4.0));

					if (Math.random() > 0.99) {
						this.recursiveDraw(
							originalX,
							originalY,
							length * (0.3 + Math.random() * 0.4),
							originalDirection +
								(-Math.PI / 6.0 +
									Math.random() * (Math.PI / 3.0)),
						);
					} else if (Math.random() > 0.96) {
						this.recursiveDraw(
							originalX,
							originalY,
							length,
							originalDirection +
								(-Math.PI / 6.0 +
									Math.random() * (Math.PI / 3.0)),
						);
						length = 0;
					}
				}
			}
			return;
		}, 20);
		return;
	}
}

class VFXStarfield extends VFXAsset {
	constructor(
		canvas: HTMLCanvasElement | null,
		img: HTMLImageElement | null,
		opacity: number,
	) {
		super(canvas, img, 0, 0, 0, 0, opacity);
	}

	draw() {
		if (this.canvas) {
			const context = this.canvas.getContext(
				'2d',
			) as CanvasRenderingContext2D;
			context.globalAlpha = this.opacity / 100;
			if (this.img) {
				const pattern = context.createPattern(this.img, 'repeat');
				if (pattern) {
					pattern.setTransform(new DOMMatrix([1,0,0,1,screenX, screenY]));
					context.fillStyle = pattern;
					context.fillRect(screenX, screenY, this.canvas.width, this.canvas.height);
				}
			}
			context.globalAlpha = 1;
			return true;
		} else return false;
	}
}

type VFX = {
	canvas: HTMLCanvasElement | null;
	particles: {
		raindrop: HTMLImageElement | null;
		snowflake: HTMLImageElement | null;
	};
	assets: VFXAsset[];
	cloudWidth: number;
	cloudHeight: number;
	windSpeed: number;
	init: (
		resources: Map<string, HTMLImageElement>,
		config: VFXConfig,
		timers: number[],
		stars: boolean
	) => void;
	updateDimensions: (width: number, height: number) => void;
	setCanvas: (paintTarget: HTMLCanvasElement) => void;
	setParticles: (
		raindrop: HTMLImageElement,
		snowflake: HTMLImageElement,
	) => void;
	addAsset: (asset: VFXAsset) => void;
	clearAssets: () => void;
	spawnCloud: (options: {
		img: HTMLImageElement;
		repeat: boolean;
		repeatImg: HTMLImageElement | null;
		windSpeed: number;
		opacity: number;
	}) => void;
	spawnRaindrop: (options: {
		img: HTMLImageElement | null;
		opacity?: number;
	}) => void;
	spawnSnowflake: (options: {
		img: HTMLImageElement | null;
		windSpeed: number;
		opacity: number;
	}) => void;
	spawnLightningBolt: () => void;
	draw: () => void;
	clearCanvas: () => void;
};

const VFX: VFX = {
	canvas: null,
	particles: {
		raindrop: null as HTMLImageElement | null,
		snowflake: null as HTMLImageElement | null,
	},
	assets: [],
	cloudWidth: 1800,
	cloudHeight: 320,
	windSpeed: 0,

	init: function (
		resources: Map<string, HTMLImageElement>,
		config: VFXConfig,
		timers: number[],
		stars
	) {
		if (stars) this.addAsset(new VFXStarfield(this.canvas, resources.get('/images/vfx/starfield.png') as HTMLImageElement, 100));
		config.clouds.forEach((cloud) => {
			this.spawnCloud({
				img: resources.get(cloud.img) as HTMLImageElement,
				repeat: cloud.repeat,
				repeatImg: cloud.repeatImg
					? (resources.get(cloud.repeatImg) as HTMLImageElement)
					: null,
				windSpeed: cloud.windSpeed,
				opacity: cloud.opacity,
			});
		});
		if (config.rain) {
			timers.push(
				window.setInterval(() => {
					VFX.spawnRaindrop({
						img: this.particles.raindrop,
						opacity: config.rain?.opacity,
					});
				}, 60 / config.rain.count),
			);
		}
		if (config.lightning) {
			timers.push(
				window.setInterval(() => {
					if (
						Math.random() >
						100 - (config.lightning?.chance as number)
					) {
						VFX.spawnLightningBolt();
					}
				}, config.lightning.interval),
			);
		}
		if (config.snow) {
			timers.push(
				window.setInterval(() => {
					VFX.spawnSnowflake({
						img: this.particles.raindrop,
						windSpeed: this.windSpeed,
						opacity: config.snow?.opacity
							? config.snow?.opacity
							: 75,
					});
				}, 60 / config.snow.count),
			);
		}
	},

	updateDimensions: function (width: number, height: number) {
		if (window.innerWidth > window.innerHeight) {
			this.cloudWidth = window.innerWidth;
			this.cloudHeight = (height / width) * window.innerWidth;
		} else {
			this.cloudWidth =
				(width / height) * ((window.innerHeight / 100) * 30);
			this.cloudHeight = (window.innerHeight / 100) * 30;
		}
	},

	setCanvas: function (paintTarget: HTMLCanvasElement) {
		this.canvas = paintTarget;
	},

	setParticles: function (
		raindrop: HTMLImageElement,
		snowflake: HTMLImageElement,
	) {
		this.particles.raindrop = raindrop;
		this.particles.snowflake = snowflake;
	},

	addAsset: function (asset: VFXAsset) {
		this.assets.push(asset);
	},

	clearAssets: function () {
		this.assets.length = 0;
	},

	spawnCloud: function (options: {
		img: HTMLImageElement;
		repeat: boolean;
		repeatImg: HTMLImageElement | null;
		windSpeed: number;
		opacity: number;
	}) {
		const cloud = new VFXCloud(
			this.canvas,
			options.img,
			0,
			0,
			this.cloudWidth,
			this.cloudHeight,
			options.opacity,
			options.windSpeed,
		);
		this.assets.push(cloud);
		if (options.repeat && options.repeatImg) {
			const repeatCloud = new VFXCloud(
				this.canvas,
				options.repeatImg,
				-this.cloudWidth,
				0,
				this.cloudWidth,
				this.cloudHeight,
				options.opacity,
				options.windSpeed,
			);
			this.assets.push(repeatCloud);
		} else if (options.repeat) {
			const repeatCloud = new VFXCloud(
				this.canvas,
				options.img,
				-this.cloudWidth,
				0,
				this.cloudWidth,
				this.cloudHeight,
				options.opacity,
				options.windSpeed,
			);
			this.assets.push(repeatCloud);
		}
	},

	spawnRaindrop: function (options: {
		img: HTMLImageElement | null;
		opacity?: number;
	}) {
		const raindrop = new VFXRaindrop(
			this.canvas,
			this.particles.raindrop,
			getRandomIntInRange(0, window.innerWidth),
			-10,
			2,
			getRandomIntInRange(16, 22),
			options.opacity ? options.opacity : 40,
		);
		this.assets.push(raindrop);
	},

	spawnSnowflake: function (options: {
		img: HTMLImageElement | null;
		windSpeed: number;
		opacity: number;
	}) {
		const canvas = this.canvas;
		const snowflakeSize = getRandomIntInRange(8, 14);
		const originX = getRandomIntInRange(
			-options.windSpeed * 30,
			window.innerWidth,
		);
		let originY = -10;
		if (originX < 0)
			originY = getRandomIntInRange(
				options.windSpeed * 10,
				window.innerWidth,
			);
		const snowflake = new VFXSnowflake(
			canvas,
			this.particles.raindrop,
			originX,
			originY,
			snowflakeSize,
			snowflakeSize,
			options.opacity,
			options.windSpeed,
		);
		this.assets.push(snowflake);
	},

	spawnLightningBolt: function () {
		if (this.canvas) {
			const x = Math.floor(
				-10.0 + Math.random() * (this.canvas.width + 20.0),
			);
			const y = Math.floor(
				5.0 + Math.random() * (this.canvas.height / 8.0),
			);
			length = this.canvas.height;
			const bolt = new VFXLightningBolt(
				this.canvas,
				null,
				x,
				y,
				0,
				0,
				100,
			);
			this.assets.push(bolt);
			bolt.recursiveDraw(x, y, length, (Math.PI * 3.0) / 2.0);
		}
	},

	draw: function () {
		this.clearCanvas();
		this.assets.forEach((asset, i) => {
			if (!asset.draw()) {
				this.assets.splice(i, 1);
			}
		});
	},

	clearCanvas: function () {
		if (this.canvas) {
			(
				this.canvas.getContext('2d') as CanvasRenderingContext2D
			).clearRect(0, 0, this.canvas.width, this.canvas.height);
		}
	},
};

export default VFX;
