import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

export const isTimestampToday = (timestamp: number): boolean => {
	const today = new Date().setHours(0, 0, 0);
	const timestampDate = new Date(timestamp).setHours(0, 0, 0);
	return timestampDate === today;
};

export const getLocalDate = (dt: number, timezone: number): Date => {
	const UTC = (dt + timezone) * 1000;
	return new Date(UTC);
};

export const formatTime = (dt: number, length: string, format: string) => {
	dayjs.extend(utc);
	if (length === 'long') {
		if (format === '12h') {
			return dayjs(dt * 1000)
				.utc()
				.format('hh:mm A');
		}
		if (format === '24h') {
			return dayjs(dt * 1000)
				.utc()
				.format('HH:mm');
		}
	}
	if (length === 'short') {
		if (format === '12h') {
			return dayjs(dt * 1000)
				.utc()
				.format('h A');
		}
		if (format === '24h') {
			return dayjs(dt * 1000)
				.utc()
				.format('H');
		}
	}
	return dayjs(dt * 1000)
		.utc()
		.format('hh:mm A');
};

export const formatDate = (dt: number, includeTime: boolean) => {
	dayjs.extend(utc);
	if (includeTime)
		return dayjs(dt * 1000).utc().format('dddd, MMMM D YYYY HH:mm:ss');
	else return dayjs(dt * 1000).utc().format('dddd, MMMM D YYYY');
};
