import { ThreeHourWeather, FiveDaysForecastData } from '../api/types';
import { formatDate } from './datetime';

export const groupForecasts = (
	forecasts: FiveDaysForecastData
): Map<string, ThreeHourWeather[]> => {
	const groupedForecasts = forecasts.list.reduce(
		(
			prev: Map<string, ThreeHourWeather[]>,
			current: ThreeHourWeather,
		): Map<string, ThreeHourWeather[]> => {
			const currentDate = formatDate(current.dt + forecasts.city.timezone, false);
			const mapItem = prev.get(currentDate);
			if (mapItem) {
				mapItem.push(current);
			} else {
				prev.set(currentDate, [current]);
			}
			return prev;
		},
		new Map<string, ThreeHourWeather[]>(),
	);
	return groupedForecasts;
};

export const formatVisibility = (
	value: number,
	units: string,
) => {
	if (units === 'metric') return value / 1000;
	if (units === 'imperial') return (value * 0.00062137).toFixed(1);
	return 0;
};
