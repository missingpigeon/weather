export const debounce = <T extends Function>(
	callback: T,
	delay: number
) => {
	let timer: ReturnType<typeof setTimeout> | null = null;
	return (...args: unknown[]) => {
		if (timer) clearTimeout(timer);
		timer = setTimeout(() => {
			callback.apply(this, args);
		}, delay);
	};
};
