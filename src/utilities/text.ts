import countryCodes from '@/json/countries.json';
import aqiLevels from '@/json/aqi-levels.json';
import units from '@/json/units.json';

export const getCountryName = (key: string) => {
	if (key in countryCodes) {
		return countryCodes[key as keyof typeof countryCodes];
	}
	else return countryCodes;
}

export const getAQILevel = (key: 1 | 2 | 3 | 4 | 5) => {
	if (key.toString() in aqiLevels) {
		return aqiLevels[key.toString() as keyof typeof aqiLevels];
	}
	else return aqiLevels.toString();
}

export const capitalizeFirstLetter = (text: string): string => {
	return text[0].toUpperCase() + text.slice(1);
};

export const getDisplayUnit = (system: string, type: string) => {
	if (system in units && type in units[system as keyof typeof units]) {
		const entry = units[system as keyof typeof units];
		if (type in entry) return entry[type as keyof typeof entry];
		else return 'e';
	}
}
