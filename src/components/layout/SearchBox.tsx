import { FocusEvent, useEffect, useState } from 'react';
import { debounce } from '@/utilities/debounce';
import { findLocation } from '@/api/geocoding';
import { Location } from '@/api/types';
import { nanoid } from 'nanoid';
import { getCountryName } from '@/utilities/text';
import Spinner from '../misc/Spinner';

const SearchBox = ({
	selectLocation,
}: {
	selectLocation: (lat: number, long: number) => void;
}) => {
	const [results, setResults] = useState<Location[]>([]);
	const [showResults, setShowResults] = useState<boolean>(false);
	const [showLoading, setShowLoading] = useState<boolean>(false);

	const handleChange = async (e: InputEvent) => {
		const query = (e.target as HTMLInputElement).value;
		if (query) {
			setShowResults(true);
			setShowLoading(true);
			try {
				const locations: Location[] = await findLocation(
					(e.target as HTMLInputElement).value,
					5,
				);
				setShowLoading(false);
				setResults(locations);
			} catch (error) {
				if (error instanceof Error) {
					console.log(error.message);
				} else
					alert('Unrecoverable error occured during data fetching');
			}
		} else {
			setResults([]);
		}
	};

	useEffect(() => {
		if (results.length === 0) setShowResults(false);
	}, [results]);

	const handleLocationSelection = (lat: number, long: number) => {
		setResults([]);
		selectLocation(lat, long);
	};

	return (
		<div className='search'>
			<img
				className='search-icon'
				src='/icons/search.png'
				alt='Search icon'
			/>
			<input
				className='search-box'
				placeholder='Search for a location...'
				onFocus={() => {
					if (results.length > 0) setShowResults(true);
				}}
				onBlur={(e: FocusEvent<HTMLInputElement>) => {
					if (
						!(
							e.relatedTarget &&
							(
								e.relatedTarget as HTMLElement
							).tagName.toLowerCase() === 'button'
						)
					) {
						setShowResults(false);
					}
				}}
				onChange={debounce(handleChange, 400)}
			></input>
			<div
				className={`search-results${
					!showResults ? ` no-results` : ''
				}${showLoading ? ` is-loading` : ''}`}
			>
				<div
					className={`search-spinner${
						showLoading ? ` is-loading` : ''
					}${results.length === 0 ? ` is-initial` : ''}`}
				>
					<Spinner />
				</div>
				<ul>
					{results.map((location) => (
						<li key={nanoid()}>
							<button
								tabIndex={0	}
								onClick={() => {
									handleLocationSelection(
										location.lat,
										location.lon,
									);
								}}
							>
								{`${location.name}${
									location.state ? ', ' + location.state : ''
								}, ${getCountryName(location.country)}`}
							</button>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export default SearchBox;
