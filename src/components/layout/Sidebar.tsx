import { useState } from 'react';
import { Place } from '@/api/types';

const Sidebar = ({
	active,
	places,
	addPlace,
	deletePlace,
	selectPlace,
	currentCoordinates,
	getCurrentLocation,
}: {
	active: boolean;
	places: { current: string | null; list: Place[] };
	addPlace: () => void;
	deletePlace: (id: string) => void;
	selectPlace: (id: string) => void;
	currentCoordinates: { lat: number; long: number };
	getCurrentLocation: () => void;
}) => {
	const [editMode, setEditMode] = useState<boolean>(false);

	const toggleEditMode = () => {
		setEditMode(!editMode);
	};

	return (
		<div className={`sidebar${active ? ' active' : ''}`}>
			<div className={'buttons'}>
				<button
					className='button'
					onClick={addPlace}
					type='button'
					title='Add the currently active location to places list'
				>
					Add
				</button>
				<button
					className='button'
					onClick={toggleEditMode}
					type='button'
					title={editMode ? 'Stop editing' : 'Edit places list'}
					disabled={!(editMode || places.list.length > 0)}
				>
					{editMode ? 'Stop editing' : 'Edit'}
				</button>
			</div>
			<ul>
				<li className='place-item'>
					<button
						onClick={getCurrentLocation}
						className='button button-select'
						type='button'
						title='Use my location'
					>
						Current location
					</button>
				</li>
				{places
					? places.list.map((place) => {
							return (
								<li
									key={place.id}
									className={`place-item${
										place.lat === currentCoordinates.lat &&
										place.long === currentCoordinates.long
											? ` active`
											: ''
									}${
										editMode && places.current !== place.id
											? ` edit`
											: ''
									}`}
								>
									<button
										className='button button-select'
										onClick={() => {
											selectPlace(place.id);
										}}
										type='button'
										title={`Latitude: ${place.lat}, Longitude: ${place.long}`}
									>
										{place.name}
									</button>
									{places.current !== place.id ? (
										<button
											onClick={() =>
												deletePlace(place.id)
											}
											className='button-delete'
											type='button'
											title='Delete location'
										>
											<img
												src='/icons/minus.png'
												alt='Delete'
											/>
										</button>
									) : (
										''
									)}
								</li>
							);
						})
					: ''}
			</ul>
		</div>
	);
};

export default Sidebar;
