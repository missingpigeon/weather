import {
	useState,
	useEffect,
	useCallback,
	ReactNode,
	useRef,
	useMemo,
} from 'react';
import {
	WeatherData,
	FiveDaysForecastData,
	ThreeHourWeather,
	AirQuality,
	Place,
} from '../../api/types';
import CurrentForecast from '../content/CurrentForecast';
import {
	getCurrentWeather,
	get5DayForecast,
	getAirQualityData,
} from '../../api/weather';
import ExtendedForecast from '../content/ExtendedForecast';
import Header from './Header';
import Sidebar from './Sidebar';
import { nanoid } from 'nanoid';
import { getCountryName } from '@/utilities/text';
import Modal from '../misc/Modal';
import LoadingScreen from '../misc/LoadingScreen';
import VFX, { constructVFX } from '@/utilities/vfx';
import { debounce } from '@/utilities/debounce';
import { AppSettings } from '@/utilities/settings';
import images from '@/json/images.json';
import Footer from './Footer';

const MainView = () => {
	const vfxImages = useRef<Map<string, HTMLImageElement>>(
		new Map<string, HTMLImageElement>(),
	);

	const [vfxImagesLoaded, setVfxImagesLoaded] = useState<boolean>(false);

	const animation = useRef<number | null>(null);

	const [appSettings, setAppSettings] = useState<AppSettings>({
		units: 'metric',
		timeFormat: '12h',
		autoRefresh: 0,
		showVFX: true,
	});

	const [weatherLoading, setWeatherLoading] = useState<boolean>(false);

	const [coordinates, setCoordinates] = useState<{
		lat: number;
		long: number;
	}>({ lat: 51.47783, long: -0.00139 });

	const [currentWeather, setcurrentWeather] = useState<WeatherData | null>(
		null,
	);

	const [showPlaces, setShowPlaces] = useState<boolean>(false);

	const [fiveDaysForecast, setFiveDaysForecast] =
		useState<FiveDaysForecastData | null>(null);

	const [currentAirQuality, setCurrentAirQuality] =
		useState<AirQuality | null>(null);

	const [places, setPlaces] = useState<{
		current: string | null;
		list: Place[];
	}>({ current: null, list: [] });

	const [showModal, setShowModal] = useState<boolean>(false);

	const [modalSettings, setModalSettings] = useState<{
		title: string;
		handleClose: (() => void) | null;
		content: ReactNode | null;
	}>({ title: '', handleClose: null, content: null });

	const openModal = (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => {
		setModalSettings({
			title: title,
			handleClose: handleClose,
			content: content,
		});
		setShowModal(true);
	};

	const closeModal = () => {
		setShowModal(false);
	};

	const getCurrentLocation = () => {
		navigator.geolocation.getCurrentPosition(
			(position) => {
				setCoordinates({
					lat: position.coords.latitude,
					long: position.coords.longitude,
				});
			},
			(error) => {
				if (error.code == error.PERMISSION_DENIED) {
					alert('Geolocation declined by user');
				} else {
					alert('Failed to get current location');
				}
			},
		);
		setShowPlaces(false);
	};

	const addPlace = () => {
		if (currentWeather) {
			const newPlace: Place = {
				id: nanoid(),
				lat: coordinates.lat,
				long: coordinates.long,
				name:
					currentWeather.name +
					', ' +
					getCountryName(currentWeather.sys.country),
			};

			const currentPlacesList = places.list;
			if (
				currentPlacesList.findIndex(
					(place) =>
						place.lat === newPlace.lat &&
						place.long === newPlace.long,
				) === -1
			) {
				if (!places.list.length) {
					setPlaces({ ...places, current: newPlace.id });
				}
				const updatedPlacesList = [...currentPlacesList, newPlace];
				setPlaces({ ...places, list: updatedPlacesList });
			}
		}
	};

	const deletePlace = (id: string) => {
		const updatedPlacesList = places.list.filter(
			(place) => place.id !== id,
		);
		setPlaces({ ...places, list: updatedPlacesList });
	};

	const selectPlace = (id: string) => {
		const place = places.list.find((item) => item.id === id);
		if (place) {
			setCoordinates({ lat: place.lat, long: place.long });
			setPlaces({ ...places, current: id });
			setShowPlaces(false);
		}
	};

	const sortForecasts = useCallback(
		(forecasts: FiveDaysForecastData) => {
			forecasts?.list.sort((a: ThreeHourWeather, b: ThreeHourWeather) => {
				return a.dt - b.dt;
			});
		},
		[coordinates],
	);

	const selectLocation = (lat: number, long: number) => {
		setCoordinates({ lat: lat, long: long });
		setShowPlaces(false);
	};

	const toggleSidebar = () => {
		setShowPlaces(!showPlaces);
	};

	useEffect(() => {
		setWeatherLoading(true);
		const promises = [] as Promise<{
			key: string;
			img: HTMLImageElement;
		}>[];
		images.forEach((key) => {
			promises.push(
				new Promise((resolve, reject) => {
					let img = new Image();
					img.src = key;
					img.onload = () => {
						resolve({ key: key, img: img });
					};
					img.onerror = () => {
						reject({ key: key, img: img });
					};
				}),
			);
		});
		Promise.all<{ key: string; img: HTMLImageElement }>(promises)
			.then((images) => {
				images.forEach((entry) => {
					vfxImages.current.set(entry.key, entry.img);
				});
				VFX.setParticles(
					vfxImages.current.get(
						'/images/vfx/raindrop.png',
					) as HTMLImageElement,
					vfxImages.current.get(
						'/images/vfx/snowflake.png',
					) as HTMLImageElement,
				);
				setWeatherLoading(false);
				setVfxImagesLoaded(true);
			})
			.catch(() => {
				setWeatherLoading(false);
			});
	}, []);

	const getWeather = useCallback(async () => {
		setWeatherLoading(true);
		try {
			const result: [WeatherData, FiveDaysForecastData, AirQuality] =
				await Promise.all([
					getCurrentWeather({
						lat: coordinates.lat,
						long: coordinates.long,
						units: appSettings.units,
					}),
					get5DayForecast({
						lat: coordinates.lat,
						long: coordinates.long,
						units: appSettings.units,
					}),
					getAirQualityData({
						lat: coordinates.lat,
						long: coordinates.long,
					}),
				]);
			setcurrentWeather(result[0]);
			sortForecasts(result[1]);
			setFiveDaysForecast(result[1]);
			setCurrentAirQuality(result[2]);
		} catch (error) {
			if (error instanceof Error) {
				console.log(error.message);
			} else alert('Unrecoverable error occured during data fetching');
		}
		setWeatherLoading(false);
	}, [coordinates, appSettings.units]);

	useEffect(() => {
		getWeather();
		if (currentWeather) VFX.windSpeed = currentWeather.wind.speed;
	}, [coordinates, appSettings.units]);

	const VFXConfig = useMemo(() => {
		if (currentWeather) {
			return constructVFX(currentWeather.weather[0].id, {
				dt: currentWeather.dt + currentWeather.timezone,
				sunrise: currentWeather.sys.sunrise + currentWeather.timezone,
				sunset: currentWeather.sys.sunset + currentWeather.timezone,
			});
		}
	}, [currentWeather?.weather[0].id, currentWeather?.dt]);

	const constructBackgroundGradient = () => {
		if (VFXConfig) {
			const colorStops: string[] = VFXConfig.sky;
			if (colorStops.length === 3)
				return `linear-gradient(to bottom, ${colorStops[0]} 0%, ${colorStops[1]} 50%, ${colorStops[2]} 100%)`;
		}
		return 'linear-gradient(to bottom, #5ab2ff 0%, #caf4ff 100%)';
	};

	const updateCanvasSize = useCallback((canvas: HTMLCanvasElement | null) => {
		if (canvas) {
			canvas.width = canvas.getBoundingClientRect().width;
			canvas.height = canvas.getBoundingClientRect().height;
		}
	}, []);

	const initCanvas = useCallback((canvas: HTMLCanvasElement | null) => {
		if (canvas) {
			updateCanvasSize(canvas);
			window.addEventListener(
				'resize',
				debounce(() => {
					updateCanvasSize(canvas);
				}, 500),
			);
			VFX.setCanvas(canvas);
		}
	}, []);

	const animate = () => {
		if (!VFX.canvas) {
			return;
		} else {
			VFX.draw();
		}
		animation.current = requestAnimationFrame(animate);
	};

	useEffect(() => {
		const timers = [] as number[];
		if (currentWeather && appSettings.showVFX && VFX.canvas) {
			if (VFXConfig) {
				VFX.init(vfxImages.current, VFXConfig.animations, timers, VFXConfig.stars);
			}
			animation.current = requestAnimationFrame(animate);
			return () => {
				if (animation.current) cancelAnimationFrame(animation.current);
				timers.forEach((id) => window.clearInterval(id));
				timers.length = 0;
				VFX.clearAssets();
				VFX.clearCanvas();
			};
		}
	}, [VFXConfig?.animations, appSettings.showVFX, vfxImagesLoaded]);

	if (currentWeather && fiveDaysForecast && currentAirQuality) {
		const weatherSummary = {
			dt: currentWeather.dt,
			timezone: currentWeather.timezone,
			name: currentWeather.name,
			description: currentWeather.weather[0].description,
			icon: currentWeather.weather[0].icon,
			temp: currentWeather.main.temp,
			pop: fiveDaysForecast.list[0].pop,
			humidity: currentWeather.main.humidity,
			feels_like: currentWeather.main.feels_like,
			temp_max: fiveDaysForecast.list[0].main.temp_max,
			temp_min: fiveDaysForecast.list[0].main.temp_min,
			wind_deg: currentWeather.wind.deg,
			wind_speed: currentWeather.wind.speed,
			wind_gust: currentWeather.wind.gust,
			sunrise: currentWeather.sys.sunrise,
			sunset: currentWeather.sys.sunset,
			pressure: currentWeather.main.pressure,
			visibility: currentWeather.visibility,
			rain_snow: {
				rain: currentWeather.rain ?? undefined,
				snow: currentWeather.snow ?? undefined,
			},
			cloudiness: currentWeather.clouds.all,
			air: currentAirQuality,
		};

		const sky = constructBackgroundGradient();

		return (
			<main style={{ background: sky }}>
				<canvas ref={initCanvas} className='weather-vfx'></canvas>
				<div
					onClick={() => {
						setShowPlaces(false);
					}}
					className={`overlay${showPlaces ? ' active' : ''}`}
				></div>
				<Header
					showPlaces={showPlaces}
					getCurrentLocation={getCurrentLocation}
					selectLocation={selectLocation}
					toggleSidebar={toggleSidebar}
				/>
				<Sidebar
					active={showPlaces}
					places={places}
					addPlace={addPlace}
					deletePlace={deletePlace}
					selectPlace={selectPlace}
					currentCoordinates={coordinates}
					getCurrentLocation={getCurrentLocation}
				/>
				<div className='main-content'>
					<div className='container'>
						<CurrentForecast
							coordinates={coordinates}
							getWeather={getWeather}
							weather={weatherSummary}
							openModal={openModal}
							closeModal={closeModal}
							settings={appSettings}
							setAppSettings={setAppSettings}
						/>
						<ExtendedForecast
							forecasts={fiveDaysForecast}
							units={appSettings.units}
							timeFormat={appSettings.timeFormat}
							timezone={weatherSummary.timezone}
						/>
						<Footer openModal={openModal} closeModal={closeModal} />
					</div>
				</div>
				{weatherLoading && <LoadingScreen />}
				<Modal
					show={showModal}
					title={modalSettings.title}
					handleClose={modalSettings.handleClose}
					children={modalSettings.content}
				/>
			</main>
		);
	} else return <LoadingScreen />;
};

export default MainView;
