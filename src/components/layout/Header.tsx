import SearchBox from './SearchBox';

export type HeaderProps = {
	showPlaces: boolean;
	getCurrentLocation: () => void;
	selectLocation: (lat: number, long: number) => void;
	toggleSidebar: () => void;
};

const Header = (props: HeaderProps) => {
	return (
		<>
			<header className='header'>
				<div className='container'>
					<button
						onClick={props.toggleSidebar}
						type='button'
						className={`button button-places${
							props.showPlaces ? ` expanded` : ''
						}`}
					>
						<img src='/icons/places.png' alt='List icon' draggable='false' />
						<span>Places</span>
					</button>
					<SearchBox selectLocation={props.selectLocation} />
					<button
						onClick={props.getCurrentLocation}
						type='button'
						className='button button-geolocate'
						title='Use my location'
					>
						<img src='/icons/geolocate.png' alt='GPS icon' draggable='false' />
					</button>
				</div>
			</header>
		</>
	);
};

export default Header;
