import { ReactNode } from 'react';
import AboutApp from '../content/AboutApp';

const Footer = ({
	openModal,
	closeModal,
}: {
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<footer className='footer'>
			<button
				className='button ghost'
				onClick={() => {
					openModal(
						'About',
						closeModal,
						<AboutApp handleClose={closeModal} />,
					);
				}}
			>
				About this app
			</button>
			<a href='https://gitlab.com/missingpigeon/weather' target='_blank' rel='external'>
				Visit GitLab repo
			</a>
		</footer>
	);
};

export default Footer;
