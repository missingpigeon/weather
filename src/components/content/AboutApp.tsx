const AboutApp = ({ handleClose }: { handleClose: () => void }) => {
	return (
		<>
			<div className='modal-content'>
				<div className='about-text'>
					<p>
						Made in 2024 by{' '}
						<a
							href='https://gitlab.com/missingpigeon'
							rel='external'
							target='_blank'
						>
							missingpigeon
						</a>
						.
					</p>
					<p>
						This app uses weather data provided by{' '}
						<a
							href='https://openweathermap.org/api'
							rel='external'
							target='_blank'
						>
							OpenWeather's Weather API
						</a>
						.
					</p>
				</div>
				<h6 className='about-subheader'>Credits</h6>
				<ul className='group-box credits'>
					<li className='credits-item'>
						<span>Gerwin van Royen's </span>
						<a
							href='https://codepen.io/Gerwinnz/pen/RVzrRG'
							rel='external'
							target='_blank'
						>
							Animated Weather Banner
						</a>
					</li>
					<li className='credits-item'>
						<span>Jarod Long's </span>
						<a
							href='https://codepen.io/jlong64/pen/kXRxpA'
							rel='external'
							target='_blank'
						>
							Canvas Ligntning Bolt
						</a>
					</li>
					<li className='credits-item'>
						<span>Jackseller's </span>
						<a
							href='https://www.deviantart.com/jackseller/art/Weather-Icons-611424540'
							rel='external'
							target='_blank'
						>
							weather icons
						</a>
					</li>
					<li className='credits-item'>
						<span>Cloud texures from </span>
						<a
							href='https://resourceboy.com/textures/cloud-textures/'
							rel='external'
							target='_blank'
						>
							Resource Boy
						</a>
					</li>
					<li className='credits-item'>
						<a
							href='https://www.syncfusion.com/downloads/metrostudio'
							rel='external'
							target='_blank'
						>
							Syncfusion Metro Studio
						</a>
						<span> icons</span>
					</li>
				</ul>
			</div>
			<div className='modal-footer'>
				<button onClick={handleClose} className='button'>
					OK
				</button>
			</div>
		</>
	);
};

export default AboutApp;
