import { formatDate, formatTime } from '@/utilities/datetime';
import { getDisplayUnit } from '@/utilities/text';

export type WeatherTileProps = {
	weather: {
		dt: number;
		main: string;
		icon: string;
		temp: number;
		temp_max: number;
		temp_min: number;
	};
	units: string;
	timeFormat: string;
	timezone: number;
};

const WeatherTile = (props: WeatherTileProps) => {
	return (
		<div
			className='weather-tile'
			title={formatDate(props.weather.dt + props.timezone, true)}
		>
			<p>
				{formatTime(
					props.weather.dt + props.timezone,
					'short',
					props.timeFormat,
				)}
			</p>
			<div className='weather-tile-icon'>
				<img
					alt={props.weather.main}
					src={`/icons/${props.weather.icon}.png`}
				/>
			</div>
			<p>{`${Math.round(props.weather.temp)}º${getDisplayUnit(props.units, 'temp')}`}</p>
		</div>
	);
};

export default WeatherTile;
