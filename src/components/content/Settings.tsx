import { AppSettings } from '@/utilities/settings';
import { useRef } from 'react';

const Settings = ({
	settings,
	setAppSettings,
	handleClose,
}: {
	settings: AppSettings;
	setAppSettings: (settings: AppSettings) => void;
	handleClose: () => void;
}) => {
	const formRef = useRef<HTMLFormElement | null>(null);

	const updateSettings = () => {
		const formElement = formRef.current;
		if (formElement) {
			const formData = new FormData(formElement);
			const units = formData.get('units') as string;
			const timeFormat = formData.get('timeFormat') as string;
			const autoRefresh = formData.get('autoRefresh')
				? parseInt(formData.get('autoRefresh') as string)
				: 0;
			const showVFX = formData.get('showVFX') ? true : false;
			if (typeof autoRefresh === 'number')
				setAppSettings({
					units,
					timeFormat,
					autoRefresh,
					showVFX
				});
		}
		handleClose();
	};

	return (
		<>
			<div className='modal-content'>
				<div className='app-settings'>
					<form ref={formRef} className='app-settings-form'>
						<div className='group-box'>
							<label className='group-box-label'>
								Units:
							</label>
							<label
								className='form-control'
								htmlFor='settingsMetric'
							>
								<input
									className='radio-button'
									id='settingsMetric'
									type='radio'
									name='units'
									value='metric'
									defaultChecked={settings.units === 'metric'}
								/>
								Metric
							</label>
							<label
								className='form-control'
								htmlFor='settingsImperial'
							>
								<input
									className='radio-button'
									id='settingsImperial'
									type='radio'
									name='units'
									value='imperial'
									defaultChecked={
										settings.units === 'imperial'
									}
								/>
								Imperial
							</label>
						</div>
						<div className='group-box'>
							<label className='group-box-label'>
								Time format:
							</label>

							<label
								className='form-control'
								htmlFor='settings12H'
							>
								<input
									className='radio-button'
									id='settings12H'
									type='radio'
									name='timeFormat'
									value='12h'
									defaultChecked={
										settings.timeFormat === '12h'
									}
								/>
								12 hours
							</label>
							<label
								className='form-control'
								htmlFor='settings24H'
							>
								<input
									className='radio-button'
									id='settings24H'
									type='radio'
									name='timeFormat'
									value='24h'
									defaultChecked={
										settings.timeFormat === '24h'
									}
								/>
								24 hours
							</label>
						</div>
						<div className='group-box'>
							<label className='group-box-label'>
								Automatically refresh:
							</label>
							<label
								className='form-control'
								htmlFor='settingsAutoRefresh'
							>
								<input
									className='radio-button'
									id='settingsAutoRefresh'
									type='radio'
									name='autoRefresh'
									value='yes'
									defaultChecked={settings.autoRefresh > 0}
								/>
								Every
							</label>
							<select className='select' name='refreshInterval'>
								<option value='300'>5 minutes</option>
								<option value='900'>15 minutes</option>
								<option value='1800'>30 minutes</option>
								<option value='3600'>1 hour</option>
								<option value='7200'>2 hours</option>
							</select>
							<label
								className='form-control'
								htmlFor='settingsNoAutoRefresh'
							>
								<input
									className='radio-button'
									id='settingsNoAutoRefresh'
									type='radio'
									name='autoRefresh'
									value='0'
									defaultChecked={settings.autoRefresh === 0}
								/>
								No
							</label>
						</div>
						<div className='group-box'>
							<label htmlFor='settingsShowVFX'>
								<input
									className='checkbox'
									id='settingsShowVFX'
									type='checkbox'
									name='showVFX'
									value='showVFX'
									defaultChecked={settings.showVFX}
								/>
								Display background effects
							</label>
						</div>
					</form>
				</div>
			</div>
			<div className='modal-footer'>
				<button
					type='button'
					onClick={updateSettings}
					className='button'
				>
					Apply
				</button>
				<button type='button' onClick={handleClose} className='button'>
					Cancel
				</button>
			</div>
		</>
	);
};

export default Settings;
