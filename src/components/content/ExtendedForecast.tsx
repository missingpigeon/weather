import { ReactElement, useMemo, useState } from 'react';
import { ThreeHourWeather, FiveDaysForecastData } from '../../api/types';
import { groupForecasts } from '../../utilities/weather';
import dayjs from 'dayjs';
import WeatherTile from './WeatherTile';

export type ExtendedForecastProps = {
	forecasts: FiveDaysForecastData | null;
	units: string;
	timeFormat: string;
	timezone: number;
};

const ExtendedForecast = (props: ExtendedForecastProps) => {
	const [activeTab, setActiveTab] = useState<string>('');
	const groupedForecasts: Map<string, ThreeHourWeather[]> | null = useMemo(
		() => (props.forecasts ? groupForecasts(props.forecasts) : null),
		[props.forecasts],
	);
	if (groupedForecasts) {
		const keys = Array.from(groupedForecasts.keys());
		if (activeTab === '') setActiveTab(keys[0]);
		const activeTabIndex = keys.findIndex((key) => activeTab === key);
		const tabs: ReactElement[] = [];
		const pages: ReactElement[] = [];
		groupedForecasts.forEach((element, key) => {
			const currentDay = dayjs().format('dddd, MMMM D YYYY');
			const tabId = `tab-${key.split(/,| /).join('')}`;
			const tabPanelId = `tab-panel-${key.split(/,| /).join('')}`;
			tabs.push(
				<li
					id={tabId}
					className={`${activeTab === key ? 'active' : ''}`}
					key={key}
					role='tab'
					aria-controls={tabPanelId}
					aria-selected={activeTab === key}
					tabIndex={activeTab === key ? 0 : -1}
				>
					<button
						type='button'
						onClick={() => {
							setActiveTab(key);
						}}
						className={`tab${activeTab === key ? ' active' : ''}`}
						title={key}
					>
						{key === currentDay
							? 'Today'
							: dayjs(key).format('ddd')}
					</button>
				</li>,
			);
			pages.push(
				<div
					id={tabPanelId}
					key={key}
					className={`daily-forecast${
						activeTab === key ? ` active` : ''
					}`}
					role='tabpanel'
					hidden={!(activeTab === key)}
					aria-expanded={activeTab === key}
					aria-labelledby={tabId}
				>
					{element.map((forecast) => (
						<WeatherTile
							key={forecast.dt}
							weather={{
								dt: forecast.dt,
								main: forecast.weather[0].main,
								icon: forecast.weather[0].icon,
								temp: forecast.main.temp,
								temp_max: forecast.main.temp_max,
								temp_min: forecast.main.temp_min,
							}}
							units={props.units}
							timeFormat={props.timeFormat}
							timezone={props.timezone}
						/>
					))}
				</div>,
			);
		});
		return (
			<div className={`tab-control extended-forecast`}>
				<label>Extended forecast</label>
				<img
					className='whats-this'
					src='/icons/whatsthis.png'
					alt='Question mark'
					title='Forecast for the next five days, in 3-hour increments'
				/>
				<ul className='extended-forecast-tabs' role='tablist'>
					{tabs}
				</ul>
				<div className='extended-forecast-paginator'>
					<button
						type='button'
						title='Previous day'
						onClick={() => {
							setActiveTab(
								keys[
									activeTabIndex > 0 ? activeTabIndex - 1 : 0
								],
							);
						}}
						disabled={activeTabIndex <= 0}
					>
						<img src='/icons/prev-page.png' alt='Prev' />
					</button>
					<p>
						{activeTab === dayjs().format('dddd, MMMM D YYYY')
							? 'Today'
							: dayjs(activeTab).format('dddd')}
					</p>
					<button
						type='button'
						title='Next day'
						onClick={() => {
							setActiveTab(
								keys[
									activeTabIndex < keys.length - 1
										? activeTabIndex + 1
										: keys.length - 1
								],
							);
						}}
						disabled={activeTabIndex >= keys.length - 1}
					>
						<img src='/icons/next-page.png' alt='Prev' />
					</button>
				</div>
				<div className='extended-forecast-pages'>{pages}</div>
			</div>
		);
	} else {
		return <div>No extended forecast available</div>;
	}
};

export default ExtendedForecast;
