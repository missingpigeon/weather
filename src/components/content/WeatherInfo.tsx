import { formatDate } from '@/utilities/datetime';

const WeatherInfo = ({
	name,
	lat,
	long,
	dt,
	handleClose,
}: {
	name: string;
	lat: number;
	long: number;
	dt: number;
	handleClose: () => void;
}) => {
	return (
		<>
			<div className='modal-content'>
				<div className='help-text'>
					<p>
						Weather data provided by{' '}
						<a href='https://openweathermap.org/' target='_blank'>
							OpenWeather
						</a>
						.
					</p>
				</div>
				<div className='groupbox'>
					<p>
						<strong>Location name: </strong>
						<span>{name}</span>
					</p>
					<p>
						<strong>Latitude: </strong>
						<span>{lat}</span>
					</p>
					<p>
						<strong>Longitude: </strong>
						<span>{long}</span>
					</p>
					<p>
						<strong>Data calculated at: </strong>
						<span>{`${formatDate(dt, true)} UTC`}</span>
					</p>
				</div>
			</div>
			<div className='modal-footer'>
				<button onClick={handleClose} className='button'>
					OK
				</button>
			</div>
		</>
	);
};

export default WeatherInfo;
