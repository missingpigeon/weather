import { capitalizeFirstLetter, getDisplayUnit } from '../../utilities/text';
import { AirQualityComponents, WeatherSummary } from '../../api/types';
import { ReactNode } from 'react';
import AirQualityWidget from '../widgets/AirQualityWidget';
import PrecipitationWidget from '../widgets/PrecipitationWidget';
import HumidityWidget from '../widgets/HumidityWidget';
import WindWidget from '../widgets/WindWidget';
import SunWidget from '../widgets/SunWidget';
import PressureWidget from '../widgets/PressureWidget';
import VisibilityWidget from '../widgets/VisibilityWidget';
import RainSnowWidget from '../widgets/RainSnowWidget';
import CloudinessWidget from '../widgets/CloudinessWidget';
import Settings from './Settings';
import { AppSettings } from '@/utilities/settings';
import WeatherInfo from './WeatherInfo';

type SummaryProps = {
	coordinates: { lat: number; long: number };
	getWeather: () => void;
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
	settings: AppSettings;
	setAppSettings: (settings: AppSettings) => void;
};

const CurrentForecast = ({
	coordinates,
	getWeather,
	weather,
	openModal,
	closeModal,
	settings,
	setAppSettings,
}: SummaryProps) => {
	if (weather === null) {
		return <div>Weather data not available :&#40;</div>;
	} else {
		const airQualityWidget: AirQualityComponents = {
			co: weather.air.list[0].components.co,
			no: weather.air.list[0].components.no,
			no2: weather.air.list[0].components.no2,
			o3: weather.air.list[0].components.o3,
			so2: weather.air.list[0].components.so2,
			pm2_5: weather.air.list[0].components.pm2_5,
			pm10: weather.air.list[0].components.pm10,
			nh3: weather.air.list[0].components.nh3,
		};
		return (
			<div className='weather-summary'>
				<div className='weather-summary-header'>
					<div className='weather-icon'>
						<img
							alt={weather.description}
							src={`/icons/${weather.icon}.png`}
						/>
					</div>
					<div className='primary-stats card'>
						<h2 className='primary-stats-name'>{weather.name}</h2>
						<p className='primary-stats-temp'>{`${Math.round(
							weather.temp,
						)}º${getDisplayUnit(settings.units, 'temp')}`}</p>
						<p className='primary-stats-description'>
							{capitalizeFirstLetter(weather.description)}
						</p>
						<p className='primary-stats-temp-info'>{`Feels like ${Math.round(
							weather.feels_like,
						)}º${getDisplayUnit(settings.units, 'temp')} | High: ${Math.round(
							weather.temp_max,
						)}º${getDisplayUnit(settings.units, 'temp')} | Low: ${Math.round(
							weather.temp_min,
						)}º${getDisplayUnit(settings.units, 'temp')}`}</p>
					</div>
					<div className='weather-summary-controls'>
						<button
							onClick={getWeather}
							className='button'
							type='button'
							title='Refresh'
						>
							<img
								alt='Refresh icon'
								src='/icons/refresh.png'
								draggable='false'
							/>
						</button>
						<button
							onClick={() => {
								openModal(
									'About weather data',
									closeModal,
									<WeatherInfo
										name={weather.name}
										lat={coordinates.lat}
										long={coordinates.long}
										dt={weather.dt + weather.timezone}
										handleClose={closeModal}
									/>,
								);
							}}
							className='button'
							type='button'
							title='Info'
						>
							<img
								alt='Info icon'
								src='/icons/info.png'
								draggable='false'
							/>
						</button>
						<button
							onClick={() => {
								openModal(
									'Settings',
									closeModal,
									<Settings
										settings={settings}
										setAppSettings={setAppSettings}
										handleClose={closeModal}
									/>,
								);
							}}
							className='button'
							type='button'
							title='Settings'
						>
							<img
								alt='Settings icon'
								src='/icons/settings.png'
								draggable='false'
							/>
						</button>
					</div>
				</div>
				<div className='secondary-stats'>
					<PrecipitationWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
					/>
					<HumidityWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
					/>
					<WindWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
						units={settings.units}
					/>
					<SunWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
						timeFormat={settings.timeFormat}
					/>
					<PressureWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
					/>
					<VisibilityWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
						units={settings.units}
					/>
					<RainSnowWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
					/>
					<CloudinessWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
					/>
					<AirQualityWidget
						weather={weather}
						openModal={openModal}
						closeModal={closeModal}
						airQualityWidget={airQualityWidget}
					/>
				</div>
			</div>
		);
	}
};

export default CurrentForecast;
