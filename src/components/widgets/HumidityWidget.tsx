import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const HumidityWidget = ({
	weather,
	openModal,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<button id='humidityWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/humidity.png'
				alt='Water drop icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-humidity'>{`${weather.humidity}%`}</p>
			</div>
			<label>Humidity</label>
		</button>
	);
};

export default HumidityWidget;
