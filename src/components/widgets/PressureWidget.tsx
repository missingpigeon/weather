import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const PressureWidget = ({
	weather,
	openModal,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<button id='pressureWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/pressure.png'
				alt='barometer icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-pressure'>{`${weather.pressure} hPa`}</p>
			</div>
			<label>Pressure</label>
		</button>
	);
};

export default PressureWidget;
