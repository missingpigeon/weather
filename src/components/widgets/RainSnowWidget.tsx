import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const RainSnowWidget = ({
	weather,
	openModal,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<button id='rainSnowWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/rainsnow.png'
				alt='Rain and snow icon'
			/>
			<div className='secondary-stats-value'>
				{weather.rain_snow.rain?.['1h'] ? (
					<p className='secondary-stats-rainsnow'>
						<span>Last 1h: </span>
						{`${weather.rain_snow.rain['1h']} mm`}
					</p>
				) : (
					''
				)}
				{weather.rain_snow.rain?.['3h'] ? (
					<p className='secondary-stats-rainsnow'>
						<span>Last 3h: </span>
						{`${weather.rain_snow.rain['1h']} mm`}
					</p>
				) : (
					''
				)}
				{weather.rain_snow.snow?.['1h'] ? (
					<p className='secondary-stats-rainsnow'>
						<span>Last 1h: </span>
						{`${weather.rain_snow.snow['1h']} mm`}
					</p>
				) : (
					''
				)}
				{weather.rain_snow.snow?.['3h'] ? (
					<p className='secondary-stats-rainsnow'>
						<span>Last 3h: </span>
						{`${weather.rain_snow.snow['1h']} mm`}
					</p>
				) : (
					''
				)}
				{!weather.rain_snow.snow && !weather.rain_snow.rain ? (
					<p className='secondary-stats-rainsnow'>0 mm</p>
				) : (
					''
				)}
			</div>
			<label>Rain and Snow</label>
		</button>
	);
};

export default RainSnowWidget;
