import { WeatherSummary } from '@/api/types';
import { getDisplayUnit } from '@/utilities/text';
import { formatVisibility } from '@/utilities/weather';
import { ReactNode } from 'react';

const VisibilityWidget = ({
	weather,
	openModal,
	closeModal,
	units,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
	units: string;
}) => {
	return (
		<button id='visibilityWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/visibility.png'
				alt='Visibility icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-visibility'>{`${formatVisibility(weather.visibility, units)} ${getDisplayUnit(units, 'visibility')}`}</p>
			</div>
			<label>Visibility</label>
		</button>
	);
};

export default VisibilityWidget;
