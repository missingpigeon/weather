import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { formatTime } from '@/utilities/datetime';

const SunWidget = ({
	weather,
	openModal,
	closeModal,
	timeFormat,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
	timeFormat: string;
}) => {
	dayjs.extend(utc);
	return (
		<button id='sunWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/sun.png'
				alt='Sun icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-sun'>
					<span>Sunrise: </span>
					{formatTime(weather.sunrise + weather.timezone, 'long', timeFormat)}
				</p>
				<p className='secondary-stats-sun'>
					<span>Sunset: </span>
					{formatTime(weather.sunset + weather.timezone, 'long', timeFormat)}
				</p>
			</div>
			<label>Sunrise & Sunset</label>
		</button>
	);
};

export default SunWidget;
