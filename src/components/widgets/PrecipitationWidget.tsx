import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const PrecipitationWidget = ({
	weather,
	openModal,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<button id='popWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/precipitation.png'
				alt='Umbrella icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-pop'>{`${Math.floor(weather.pop * 100)}%`}</p>
			</div>
			<label>Precipitation</label>
		</button>
	);
};

export default PrecipitationWidget;
