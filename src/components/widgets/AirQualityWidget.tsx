import { getAQILevel } from '@/utilities/text';
import AirQualityDetails from './AirQualityDetails';
import { AirQualityComponents, WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const AirQualityWidget = ({
	weather,
	openModal,
	airQualityWidget,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	airQualityWidget: AirQualityComponents;
	closeModal: () => void;
}) => {
	return (
		<button
			id='airWidget'
			className='card secondary-stats-item'
			onClick={() => {
				openModal(
					'Air Quality',
					() => {
						closeModal();
					},
					<AirQualityDetails stats={airQualityWidget} handleClose={closeModal} />,
				);
			}}
		>
			<img
				className='secondary-stats-icon'
				src='/icons/airquality.png'
				alt='Leaf icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-air'>
					<span>AQI: </span>
					{`${weather.air.list[0].main.aqi}`}
				</p>
				<p className='secondary-stats-air'>
					{getAQILevel(weather.air.list[0].main.aqi)}
				</p>
			</div>
			<label>Air Quality</label>
		</button>
	);
};

export default AirQualityWidget;
