import { AirQualityComponents } from '@/api/types';
import { MouseEventHandler } from 'react';

const AirQualityDetails = ({
	stats,
	handleClose,
}: {
	stats: AirQualityComponents;
	handleClose: () => void;
}) => {
	return (
		<>
			<div className='modal-content'>
				<div className='help-text'>
					<p>
						For more information on this data,{' '}
						<a
							title='OpenWeather Air Pollution API'
							target='_blank'
							href='https://openweathermap.org/api/air-pollution'
						>
							click here
						</a>
						.
					</p>
				</div>
				<ul className='stats'>
					<li className='stats-item'>
						<p>
							{stats.co} μg/m<sup>3</sup>
						</p>
						<label title='Carbon monoxide'>CO</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.no} μg/m<sup>3</sup>
						</p>
						<label title='Nitrogen monoxide'>NO</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.no2} μg/m<sup>3</sup>
						</p>
						<label title='Nitrogen dioxide'>
							NO<sub>2</sub>
						</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.o3} μg/m<sup>3</sup>
						</p>
						<label title='Ozone'>
							O<sub>3</sub>
						</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.so2} μg/m<sup>3</sup>
						</p>
						<label title='Sulphur dioxide'>
							SO<sub>2</sub>
						</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.nh3} μg/m<sup>3</sup>
						</p>
						<label title='Ammonia'>
							NH<sub>3</sub>
						</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.pm2_5} μg/m<sup>3</sup>
						</p>
						<label title='Fine particles matter'>
							PM<sub>2.5</sub>
						</label>
					</li>
					<li className='stats-item'>
						<p>
							{stats.pm10} μg/m<sup>3</sup>
						</p>
						<label title='Coarse particulate matter'>
							PM<sub>10</sub>
						</label>
					</li>
				</ul>
			</div>
			<div className='modal-footer'>
				<button onClick={handleClose} className='button'>
					OK
				</button>
			</div>
		</>
	);
};

export default AirQualityDetails;
