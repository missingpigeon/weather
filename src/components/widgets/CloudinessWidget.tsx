import { WeatherSummary } from '@/api/types';
import { ReactNode } from 'react';

const CloudinessWidget = ({
	weather,
	openModal,
	closeModal,
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
}) => {
	return (
		<button id='cloudWidget' className='card secondary-stats-item'>
			<img
				className='secondary-stats-icon'
				src='/icons/cloudiness.png'
				alt='Cloudiness icon'
			/>
			<div className='secondary-stats-value'>
				<p className='secondary-stats-cloud'>{`${weather.cloudiness}%`}</p>
			</div>
			<label>Cloudiness</label>
		</button>
	);
};

export default CloudinessWidget;
