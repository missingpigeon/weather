import { WeatherSummary } from '@/api/types';
import { getDisplayUnit } from '@/utilities/text';
import { ReactNode } from 'react';

const WindWidget = ({
	weather,
	openModal,
	closeModal,
	units
}: {
	weather: WeatherSummary;
	openModal: (
		title: string,
		handleClose: () => void,
		content: ReactNode,
	) => void;
	closeModal: () => void;
	units: string;
}) => {
	return (
		<button id='windWidget' className='card secondary-stats-item'>
			<div className='wind-compass'>
				<img
					className='wind-compass-background'
					src='/images/wind-compass.png'
					alt='Wind icon'
				/>
				<img
					className='wind-compass-arrow'
					src='/images/wind-arrow.png'
					alt='Wind icon'
					style={{
						transform: `rotate(${weather.wind_deg}deg)`,
					}}
				/>
				<span className='wind-compass-value'>{`${weather.wind_deg}º`}</span>
			</div>

			<div className='secondary-stats-value'>
				<p className='secondary-stats-wind'>
					<span>Wind: </span>
					{`${weather.wind_speed} ${getDisplayUnit(units, 'wind')}`}
				</p>
				<p className='secondary-stats-wind'>
					<span>Gusts: </span>
					{weather.wind_gust ? `${weather.wind_gust} ${getDisplayUnit(units, 'wind')}` : 'N/A'}
				</p>
			</div>
			<label>Wind</label>
		</button>
	);
};

export default WindWidget;
