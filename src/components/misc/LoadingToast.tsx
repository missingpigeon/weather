import Spinner from './Spinner';

const LoadingToast = ({ message }: { message: string }) => {
	return (
		<div className='toast'>
            <Spinner />
			<div className='toast-content'>
                <p className='toast-message'>{message}</p>
            </div>
		</div>
	);
};
