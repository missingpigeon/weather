import { PropsWithChildren, useEffect } from 'react';
import { createPortal } from 'react-dom';

const Modal = ({
	show,
	title,
	handleClose,
	children,
}: PropsWithChildren<{
	show: boolean;
	title: string | '';
	handleClose: (() => void) | null;
}>) => {
	const modalRoot = document.getElementById('modal-root');
	useEffect(() => {
		if (handleClose) {
			const closeOnEsc = (e: KeyboardEvent) =>
				e.key === 'Escape' ? handleClose() : null;
			document.body.addEventListener('keydown', closeOnEsc);
			return () => {
				document.body.removeEventListener('keydown', closeOnEsc);
			};
		}
	}, [handleClose]);
	if (!show || !modalRoot) return null;
	else
		return createPortal(
			<>
				<div
					onClick={handleClose ? handleClose : () => null}
					className='modal-backdrop'
				>
					<div className='modal' onClick={(e) => e.stopPropagation()}>
						<div className='modal-header'>
							<h5 className='modal-title'>{title}</h5>
							<button
								onClick={handleClose ? handleClose : () => null}
								className='button modal-close'
							>
								<img src='/icons/close.png' alt='Close' draggable='false' />
							</button>
						</div>
						{children}
					</div>
				</div>
			</>,
			modalRoot,
		);
};

export default Modal;
