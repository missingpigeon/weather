import Spinner from './Spinner';

const LoadingScreen = () => {
	return (
		<div className='loading-screen'>
			<div className='loading-screen-content'>
				<p>Initializing...</p>
				<Spinner />
			</div>
		</div>
	);
};

export default LoadingScreen;
