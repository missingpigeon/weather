import { Location } from "./types";

export const findLocation = async (query: string, limit?: number): Promise<Location[]> => {
    const url=`${import.meta.env.VITE_GEOCODING_API_URL}/direct?q=${query}${limit? '&limit=' + limit : ''}&appid=${import.meta.env.VITE_WEATHER_API_KEY}`;
    try {
        const response = await fetch(url);
        if (!response.ok) {
            return Promise.reject(new Error('Bad response from API'));
        } else {
            const results: Location[] = await response.json();
            return results;
        }
    }
    catch {
        return Promise.reject(new Error('Network error'));
    }
}