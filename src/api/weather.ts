import { WeatherData, FiveDaysForecastData, AirQuality } from './types';

const makeAPIRequest = async <T>(url: string): Promise<T> => {
	try {
		const response = await fetch(url);
		if (!response.ok) {
			return Promise.reject(new Error('Bad response from API'));
		} else return (await response.json()) as T;
	} catch {
		return Promise.reject(new Error('Network error'));
	}
};

export const getCurrentWeather = async (params: {
	lat: number;
	long: number;
	units?: string;
}): Promise<WeatherData> => {
	const url = `${import.meta.env.VITE_WEATHER_API_URL}/weather?lat=${
		params.lat
	}&lon=${params.long}&units=${
		params.units ? params.units : 'metric'
	}&APPID=${import.meta.env.VITE_WEATHER_API_KEY}`;
	return makeAPIRequest<WeatherData>(url);
};

export const get5DayForecast = async (params: {
	lat: number;
	long: number;
	units?: string;
}): Promise<FiveDaysForecastData> => {
	const url = `${import.meta.env.VITE_WEATHER_API_URL}/forecast?lat=${
		params.lat
	}&lon=${params.long}&units=${
		params.units ? params.units : 'metric'
	}&APPID=${import.meta.env.VITE_WEATHER_API_KEY}`;
	return makeAPIRequest<FiveDaysForecastData>(url);
};

export const getAirQualityData = async (params: {
	lat: number;
	long: number;
}): Promise<AirQuality> => {
	const url = `${import.meta.env.VITE_WEATHER_API_URL}/air_pollution?lat=${
		params.lat
	}&lon=${params.long}&APPID=${import.meta.env.VITE_WEATHER_API_KEY}`;
	return makeAPIRequest<AirQuality>(url);
};

