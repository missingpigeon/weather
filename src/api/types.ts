export type Coord = {
	lon: number;
	lat: number;
};

export type Weather = {
	id: number;
	main: string;
	description: string;
	icon: string;
};

export type Main = {
	temp: number;
	feels_like: number;
	temp_min: number;
	temp_max: number;
	pressure: number;
	humidity: number;
	sea_level: number;
	grnd_level: number;
};

export type Wind = {
	speed: number;
	deg: number;
	gust: number;
};

export type RainSnow = {
	[key in '1h' | '3h']: number;
};

export type Clouds = {
	all: number;
};

export type Sys = {
	type: number;
	id: number;
	country: string;
	sunrise: number;
	sunset: number;
};

// Current weather API
export type WeatherData = {
	coord: Coord;
	weather: Weather[];
	base: string;
	main: Main;
	visibility: number;
	wind: Wind;
	rain?: RainSnow;
	snow?: RainSnow;
	clouds: Clouds;
	dt: number;
	sys: Sys;
	timezone: number;
	id: number;
	name: string;
	cod: number;
};

export type ThreeHourWeather = {
	dt: number;
	main: Main;
	weather: Weather[];
	clouds: Clouds;
	wind: Wind;
	visibility: number;
	pop: number;
	rain: { '3h': number };
	city: {
		coord: Coord;
		sunrise: number;
		sunset: number;
	};
};

// 3-hour 5 days Forecast API
export type FiveDaysForecastData = {
	cnt: number;
	list: ThreeHourWeather[];
	city: {
		timezone: number;
	}
};

export type WeatherSummary = {
	dt: number;
	timezone: number;
	name: string;
	description: string;
	icon: string;
	temp: number;
	pop: number;
	humidity: number;
	feels_like: number;
	temp_max: number;
	temp_min: number;
	wind_deg: number;
	wind_speed: number;
	wind_gust: number;
	sunrise: number;
	sunset: number;
	pressure: number;
	visibility: number;
	cloudiness: number;
	rain_snow: { rain?: RainSnow; snow?: RainSnow };
	air: AirQuality;
};

// Air Pollution API
export type AirQualityComponents = {
	co: number;
	no: number;
	no2: number;
	o3: number;
	so2: number;
	pm2_5: number;
	pm10: number;
	nh3: number;
};

export type AirQuality = {
	coord: {
		lat: number;
		long: number;
	};
	list: {
		dt: number;
		main: {
			aqi: 1 | 2 | 3 | 4 | 5;
		};
		components: AirQualityComponents;
	}[];
};

// Gecocoding API
export type Location = {
	name: string;
	local_names: {
		[language: string]: string;
	};
	lat: number;
	lon: number;
	country: string;
	state?: string;
};

// Places list
export type Place = {
	id: string;
	lat: number;
	long: number;
	name: string;
};
